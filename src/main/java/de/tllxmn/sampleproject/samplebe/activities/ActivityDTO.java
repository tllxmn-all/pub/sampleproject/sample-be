package de.tllxmn.sampleproject.samplebe.activities;


public class ActivityDTO {

    Long id;
    String name;
    String description;

    public Long getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ActivityDTO(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public ActivityDTO(){}
}

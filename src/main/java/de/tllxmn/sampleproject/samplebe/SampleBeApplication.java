package de.tllxmn.sampleproject.samplebe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleBeApplication.class, args);
	}

}

package de.tllxmn.sampleproject.samplebe.activities;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ActivityController {

    private List<ActivityDTO> activitiesDummy = Arrays.asList(
            new ActivityDTO(0L, "Watch a movie",
                    "Check out the current program of your nearest cinema, buy some popcorn and enjoy the show."),
            new ActivityDTO(1L, "Go hiking",
                    "Get yourself out into the nature and get some fresh air and exercise."),
            new ActivityDTO(2L, "Bake a cake",
                    "Baking a cake is fun and to top it off, you can treat yourself to a piece when you're done."),
            new ActivityDTO(3L, "Listen to music",
                    "Put on your favourite playlist or discover some new tracks."));

    @RequestMapping("/activities")
    public List<ActivityDTO> get(){
        return this.activitiesDummy;
    }

    @RequestMapping("/activities/{id}")
    public ActivityDTO getById(@PathVariable("id")int id){
        return this.activitiesDummy.get(id);
    }
}
